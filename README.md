# jsxE6 

Sublime Text syntax and theme optimized for:
- javascript with ES6 features
- React-flavoured JSX syntax

### Usage 

To install manually for Sublime Text 3 on Mac OS, copy:

    jsxE6.tmLanguage #  syntax
    Monokai\ Phoenix\ jsxE6.tmTheme  # theme

under:

    /Users/{user}/Library/Application Support/Sublime Text 3/Packages/User

Open a javascript file (`.js`,`.jsx`,`.es6`...).

View / Syntax / Open all with current extention as... / jsxE6

Sublime / Preferences / Color Scheme / User / Monokai Phoenix jsxE6


### Packaging 

To package this properly, read [the docs](http://docs.sublimetext.info/en/latest/extensibility/packages.html).

### Credits 

Bits of codes were taken from [JavaScript Next](https://github.com/Benvie/JavaScriptNext.tmLanguage) 
and [Sublime React](https://github.com/reactjs/sublime-react/tree/master/syntax/jsx/tmLanguage).



